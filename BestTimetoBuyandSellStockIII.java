class Solution {
    public int maxProfit(int[] prices) {
        int buy1 = Integer.MIN_VALUE; // Max profit after buying the first stock
        int sell1 = 0; // Max profit after selling the first stock
        int buy2 = Integer.MIN_VALUE; // Max profit after buying the second stock
        int sell2 = 0; // Max profit after selling the second stock
        
        for (int price : prices) {
            buy1 = Math.max(buy1, -price); // Update buy1 to the maximum of current buy1 and the profit after buying the stock
            sell1 = Math.max(sell1, buy1 + price); // Update sell1 to the maximum of current sell1 and the profit after selling the stock once
            buy2 = Math.max(buy2, sell1 - price); // Update buy2 to the maximum of current buy2 and the profit after buying the stock the second time
            sell2 = Math.max(sell2, buy2 + price); // Update sell2 to the maximum of current sell2 and the profit after selling the stock twice
        }
        
        return sell2; // Return the maximum profit after two transactions
    }
}
